package variant_1;

import com.company.Calculator;
import com.company.Parser;
import org.junit.Before;
import org.junit.Test;


import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;

public class PositiveTests {

    private Parser parser;
    private Calculator calculator;



    ArrayList<String> output = new ArrayList<>();

    @Before
    public void initiate(){
        parser = new Parser();
        calculator = new Calculator();
    }

    @Test
    public void additionTest() { // проверяем то по сути как работает парсер

        String[] additionalStr = {"4 + 5", "6.7+10", "895438765 + 975737356"};
        double[] additionalDouble = {4, 5, 6.7, 10, 895438765, 975737356}; // можно предварительно высчитать и здесь не считать уже
        double result;
        double ourResult;

        for (int i = 0, j = 0; i < additionalStr.length; i++, j += 2) {

            // программа подсчитывает со строки
            output = parser.convertToPolishNotation(additionalStr[i]);
            result = calculator.calculate(output);

            // подсчитаем сами
            ourResult = additionalDouble[j] + additionalDouble[j + 1];

            //сравним результаты
            //assertEquals("Сложение не верно", result, ourResult, 0);
            assertThat("Сложение не верно",result, is(ourResult));

            output.clear();

        }
    }


    @Test
    public void substractionTest () {

        String[] subsctractionStr = {"5 - 3", "12.5 - 7.2", "975737356 - 895438765"};
        double[] subsctractionDouble = {5, 3, 12.5, 7.2, 975737356, 895438765};
        double result;
        double ourResult;

        for (int i = 0, j = 0; i < subsctractionStr.length; i++, j += 2) {

            // программа подсчитывает со строки
            output = parser.convertToPolishNotation(subsctractionStr[i]);
            result = calculator.calculate(output);

            // подсчитаем сами
            ourResult = subsctractionDouble[j] - subsctractionDouble[j + 1];

            //сравним результаты
            assertEquals("Вычитание не верно", result, ourResult, 0);

            output.clear();

        }
    }


    @Test
    public void multiplicationTest () {

        String[] multiplicationStr = {"5 * 3", "12.5 * 7.2", "975737 * 895438"};
        double[] multiplicationDouble = {5, 3, 12.5, 7.2, 975737, 895438};
        double result;
        double ourResult;

        for (int i = 0, j = 0; i < multiplicationStr.length; i++, j += 2) {

            // программа подсчитывает со строки
            output = parser.convertToPolishNotation(multiplicationStr[i]);
            result = calculator.calculate(output);

            // подсчитаем сами
            ourResult = multiplicationDouble[j] * multiplicationDouble[j + 1];

            //сравним результаты
            assertEquals("Умножение не верно", result, ourResult, 0);

            output.clear();

        }

    }


    @Test
    public void divisionTest () {

        String[] divisionStr = {"5 / 3", "12.5 / 7.2", "975737 / 895438", "5 / 0"};
        double[] divisionDouble = {5, 3,  12.5, 7.2,  975737, 895438,  5, 0};
        double result;
        double ourResult;

        for (int i = 0, j = 0; i < divisionStr.length; i++, j += 2) {

            if(divisionDouble[j+1]==0){ // когда нашли 0 вторым операндом
                //assertNotEquals("На ноль делить нельзя",0, divisionDouble[j+1],0 );
                throw new IllegalArgumentException("На ноль делить нельзя");
            }

            // программа подсчитывает со строки
            output = parser.convertToPolishNotation(divisionStr[i]);
            result = calculator.calculate(output);

            // подсчитаем сами
            ourResult = divisionDouble[j] / divisionDouble[j + 1];

            //сравним результаты
            assertEquals("Деление не верно", result, ourResult, 0);

            output.clear();

        }

    }

    @Test
    public void mixedOperations() {

        String mixedStr = "(6.7+10) * 895437 - 97573";
        double[] mixedDouble = {6.7, 10, 895437, 97573}; // можно предварительно высчитать и здесь не считать уже
        double result;
        double ourResult;

        // программа подсчитывает со строки
        output = parser.convertToPolishNotation(mixedStr);
        result = calculator.calculate(output);

        // подсчитаем сами
        ourResult = (mixedDouble[0] + mixedDouble[1]) * mixedDouble[2] - mixedDouble[3];

        //сравним результаты
        //assertEquals("Сложение не верно", result, ourResult, 0);
        assertThat("Операция не верна", result, is(ourResult));

        output.clear();

    }









    /*public void operate (double[] operandsStr,  ){

        double result1;
        double ourResult1;

        for (int i = 0, j = 0; i < operandsStr.length; i++, j += 2) {

            // программа подсчитывает со строки
            output = parser.convertToPolishNotation(operandsStr[i]);
            result = calculator.calculate(output);

            // подсчитаем сами
            switch() // здесь нужен switch на 4 пункта
            ourResult1 = additionalDouble[j] + additionalDouble[j + 1];

            //сравним результаты
            switch() // и здесь
            assertEquals(result1, ourResult1, 0);

            output.clear();
        }
    }*/



}

package variant_1;

import com.company.InputChecker;
import org.junit.Before;
import org.junit.Test;

public class NegativeTests {


    InputChecker inputChecker;

    @Before
    public void initiate(){

        inputChecker = new InputChecker();
    }



    @Test
    public void doubleOperator(){

        inputChecker.checkInputString("5 ++ 4");
    }

    @Test
    public void separatorAfterOperator(){

        inputChecker.checkInputString("78 +, 23");
    }

    @Test
    public void lacksBraces(){

        inputChecker.checkInputString("(927 + 8) / (11 + 54) + 12)");
    }

    @Test
    public void unaryMinus(){

        inputChecker.checkInputString("-7 + 11");
    }

    @Test
    public void missedOperators(){

        inputChecker.checkInputString("7 45");
    }

    @Test
    public void firstMinusBeforeBrace(){

        inputChecker.checkInputString("-(6.7 + 10 - 4)/(1+1*2)+1");
    }

    @Test
    public void minusBetweenBracketAndOperator(){

        inputChecker.checkInputString("(6.7 + 10 - 4)/-(1+1*2)+1");
    }

    @Test
    public void missedOpenBracket(){

        inputChecker.checkInputString("7)+(8");
    }







}

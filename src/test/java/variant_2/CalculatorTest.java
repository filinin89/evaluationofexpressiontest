package variant_2;

import com.company.Calculator;
import com.company.Parser;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CalculatorTest {

    private Calculator calculator;


    @Before
    public void initiate(){
        calculator = new Calculator();
    }

    @Test
    public void additionTest() {

        double result;

        String additionalStr ="89543 97573 + 11.5 + 0.0001 +";
        ArrayList<String> expArr = new ArrayList<>(Arrays.asList(additionalStr.split(" "))); // тестовую строку нужно представить в виде списка
        double outResult = 187127.5001;

        result = calculator.calculate(expArr);
        assertThat("Сложение не верно",outResult, is(result));

    }

    @Test
    public void substractionTest() {

        double result;

        String additionalStr ="1089543 97573 - 11.5 - 0.0001 -";
        ArrayList<String> additionalExpArr = new ArrayList<>(Arrays.asList(additionalStr.split(" "))); // тестовую строку нужно представить в виде списка
        double outResult = 991958.4999;

        result = calculator.calculate(additionalExpArr);
        assertThat("Вычитание не верно",outResult, is(result));

    }

    @Test
    public void multiplyTest() {

        double result;

        String additionalStr ="895 975 * 11.5 * 0.0001 *";
        ArrayList<String> additionalExpArr = new ArrayList<>(Arrays.asList(additionalStr.split(" "))); // тестовую строку нужно представить в виде списка
        double outResult = 895 * 975 * 11.5 * 0.0001;

        result = calculator.calculate(additionalExpArr);
        assertThat("Умножение не верно",outResult, is(result));

    }

    @Test
    public void divisionTest() {

        double result;
        String additionalStr ="8009543 8757 / 0.0001 /";
        ArrayList<String> additionalExpArr = new ArrayList<>(Arrays.asList(additionalStr.split(" "))); // тестовую строку нужно представить в виде списка
        double outResult = 9146446.271554185;

        result = calculator.calculate(additionalExpArr);
        assertThat("Деление не верно",outResult, is(result));

    }


    @Test
    public void mixedTest() {

        double result;
        String mixedStr = "6.7 10 + 895437 * 97573 -";
        ArrayList<String> additionalExpArr = new ArrayList<>(Arrays.asList(mixedStr.split(" ")));
        double outResult = (6.7+10) * 895437 - 97573;

        result = calculator.calculate(additionalExpArr);
        assertThat("Операция не верна", outResult, is(result));

    }
}

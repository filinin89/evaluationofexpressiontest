package variant_2;

import com.company.Main;
import org.junit.Test;

public class MainTest {

    @Test // проверим как в main вызываются други методы и как программа работает с примером
    public void mainTest(){
        String[] args = new String[1];
        args[0] = "(6.7 + 10 - 4)/(1+1*2)+1";
        Main.main(args);
    }

}

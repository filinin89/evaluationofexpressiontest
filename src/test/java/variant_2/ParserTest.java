package variant_2;

import com.company.Parser;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ParserTest {

    private Parser parser;
    private String result;

    @Before
    public void initiate(){
        parser = new Parser();

    }


    @Test
    public void parseTest1() {

        String additionalStr = "6.7+ 106.7 + 10 - 2132";
        String outResult = "6.7 106.7 + 10 + 2132 -"; // результат стороннего конвертера

        // переводим в польскую нотацию
        result = parser.convertToPolishNotation(additionalStr).toString();
        result = result.replace("[", "").replace(",", "").replace("]", "");

        assertThat("Неверный преевод в польскую нотацию",result, is(outResult));
    }

    @Test
    public void parseTest2() {

        String additionalStr = "(6.7+ 106.7) / (10 - 2132)";
        String outResult = "6.7 106.7 + 10 2132 - /"; // результат стороннего конвертера

        // переводим в польскую нотацию
        result = parser.convertToPolishNotation(additionalStr).toString();
        result = result.replace("[", "").replace(",", "").replace("]", "");

        assertThat("Неверный преевод в польскую нотацию",result, is(outResult));
    }

    @Test
    public void parseTest3() {

        String additionalStr = "(6.7+ 106.7) / (10 - 2132) + 78";
        String outResult = "6.7 106.7 + 10 2132 - / 78 +"; // результат стороннего конвертера

        // переводим в польскую нотацию
        result = parser.convertToPolishNotation(additionalStr).toString();
        result = result.replace("[", "").replace(",", "").replace("]", "");

        assertThat("Неверный преевод в польскую нотацию",result, is(outResult));
    }

    @Test
    public void parseTest4() {

        String additionalStr = "(((6.7+ 106.7) / (10 - 2132) + 78) * (102456 - 71.91 / 12 - 15*9 + 145))/10 + 1";
        String outResult = "6.7 106.7 + 10 2132 - / 78 + 102456 71.91 12 / - 15 9 * - 145 + * 10 / 1 +"; // результат стороннего конвертера

        // переводим в польскую нотацию
        result = parser.convertToPolishNotation(additionalStr).toString();
        result = result.replace("[", "").replace(",", "").replace("]", "");

        assertThat("Неверный преевод в польскую нотацию",result, is(outResult));
    }




}

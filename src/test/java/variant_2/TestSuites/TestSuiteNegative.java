package variant_2.TestSuites;

import com.company.InputChecker;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import variant_2.InputCheckerTest;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        InputCheckerTest.class
})

public class TestSuiteNegative { }

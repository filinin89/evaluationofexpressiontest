package variant_2.TestSuites;

import com.company.Main;
import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import variant_2.InputCheckerTest;
import variant_2.MainTest;
import variant_2.TestSuites.TestSuiteNegative;
import variant_2.TestSuites.TestSuitePositive;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        TestSuitePositive.class,
        TestSuiteNegative.class,
        MainTest.class
})

public class TestRunner {



}

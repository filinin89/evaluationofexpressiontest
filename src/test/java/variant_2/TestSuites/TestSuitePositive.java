package variant_2.TestSuites;

import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import variant_2.CalculatorTest;
import variant_2.ParserTest;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        ParserTest.class,
        CalculatorTest.class

})

public class TestSuitePositive { }

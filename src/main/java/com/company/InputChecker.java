package com.company;

public class InputChecker {


    private String cExpression; // checkExpression
    private char[] cExpressionSeq;


    private String digits = "0123456789";
    private String operators = "+-*/%"; // - отсюда исключим для унарного

    public boolean checkInputString(String expression){
        int digitCounter=0, operatorCounter=0, parenthesesOpenCounter=0, parenthesesCloseCounter=0;

        cExpression = expression.replace(" ", ""); // уберем пробелы, тем самым еще и избавимся от ошибки наличия пробела между цифрами,  если число по ошибке записано 4 4

        // убеждаемся, что после числа идет оператор
        cExpressionSeq = cExpression.toCharArray();
        for(int i=0; i<cExpression.length(); i++){

            // проверка на унарный минус
            if (cExpressionSeq[i]=='-' && digits.contains(Character.toString(cExpressionSeq[i+1])) ){    //cExpressionSeq[i+1] == '(' || cExpressionSeq[i-1] == '(' ||
                if(i==0){ // для первого элемента в массиве предыдущий смотреть нельзя
                    System.out.println("Проверьте правильность выражения - унарный минус");
                    return false;
                }
                if(!digits.contains(Character.toString(cExpressionSeq[i-1]))){
                    System.out.println("Проверьте правильность выражения - унарный минус");
                    return false;
                }
            }

            if(operators.contains(Character.toString(cExpressionSeq[i])) && i == 0 && cExpressionSeq[i+1]=='('){
                System.out.println("Ошибка: Оператор в начале выражения перед ( ");
                return false;
            }

            if(operators.contains(Character.toString(cExpressionSeq[i])) && ((cExpressionSeq[i-1] == '(' || cExpressionSeq[i-1] == ')') && operators.contains(Character.toString(cExpressionSeq[i+1])) || (cExpressionSeq[i+1] == '(' || cExpressionSeq[i+1] == ')') && operators.contains(Character.toString(cExpressionSeq[i+1])))){
                System.out.println("Нельзя использовать оператор между скобкой и другим оператором");
                return false;
            }

            if(Character.isDigit(cExpressionSeq[i]) ) {
                digitCounter++;
                continue; //пройдем до символа не цифра и не разделитель
            }else { // если не цифра, то могут быть символы разделения дробной части
                if(cExpressionSeq[i] == '.' || cExpressionSeq[i] == ','){ // если разделитель, то предыдущий и последущий символ - цифра
                    if(!(Character.isDigit(cExpressionSeq[i-1]) && Character.isDigit(cExpressionSeq[i+1]))){
                        System.out.println("Вам следует использовать символы отделения дробной части только между числами");
                        return false;
                    }
                }else {
                    if (cExpressionSeq[i] == '(' || cExpressionSeq[i] == ')'){
                        // проверка на пналичие всех скобок
                        if(cExpressionSeq[i] == '(') parenthesesOpenCounter++;
                        if(cExpressionSeq[i] == ')'){
                            if(parenthesesOpenCounter==0){
                                System.out.println("Выражение некорректно, вы забыли открывающую скобку");
                                return false;
                            }
                            parenthesesCloseCounter++;
                        }
                        continue; // если не скобки
                    }
                        // есть оператор , проверим что он один
                        operatorCounter++;
                        if (operators.replace("-", "").indexOf(cExpressionSeq[i + 1]) != -1) { // заисключением унарного -
                            System.out.println("Вам следует использовать только один оператор между двумя числами");
                            return false;
                        }


                        // минус перед скобкой ни к кому не относящийся так: "-(6.7 + 10 - 4)/(1+1*2)+1" тоже надо обработать


                }


            }





        }


        int parenthesesCounter = parenthesesOpenCounter - parenthesesCloseCounter;
        if(parenthesesCounter !=0){
            System.out.println("Вы забыли скобку или поставили лишнюю, проверьте выражение");
            return false;
        }

        if(operatorCounter == 0){
            System.out.println("Вы забыли написать операторы");
            return false;
        }


        System.out.println("Выражение успешно прошло проверку!"); return true;


        // проверка, что между цифрами только операторы,
        // проверка, что есть числа в выражении и есть

        // можно конечно и более простую проверку - все допустимые символы в выражении записать в строку и сверять с ней при проходе по каждому символу выражения
        // но так мы смоем только сказать, что используются недопустимые символы





    }

}

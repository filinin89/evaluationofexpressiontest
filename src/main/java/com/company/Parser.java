package com.company;

import java.util.ArrayList;
import java.util.Stack;
import java.util.StringTokenizer;

public class Parser {

    private String arithmeticOperators = "+-*/^";
    private String allOperators = "() " + arithmeticOperators;
    char[] pExpressionSeq;



    // расстановка приоритетов операциям
    private byte priority(String s)
    {
        byte result= 4;
        switch (s)
        {
            case "(":
            case ")":
                result= 0; break;
            case "+":
            case "-":
                result= 1; break;
            case "*":
            case "/":
                result= 2;
        }
        return result;
    }


    public ArrayList<String> convertToPolishNotation(String input)
    {
        input = input.replace(" ", "");
        ArrayList<String> output = new ArrayList<>();
        Stack<String> stack = new Stack<>();
        pExpressionSeq = input.toCharArray(); // если будем делить на символы, то числа будут разбиваться на отдельные цифры. Поэтому я решил использовать строковые токены
        //String tokens[] = expression.split("[+,-,*,/,(,)]");// чтоб не использовать String.valueOf() // ненадежный разделитель, пробелы не всегда
        StringTokenizer tokenizer = new StringTokenizer(input, allOperators, true); // разделит на токены по оператором, еще их и оставит на своих местах, самое то для чисел(не цифр)


        String prev = "";
        while (tokenizer.hasMoreTokens()){ // идем по всему разобранному выражению
            int i=0 ;
            String token = tokenizer.nextToken();


            if(Character.isDigit(token.charAt(0))){
                output.add(token);  // цифру в выходную строку



            }else{
                // работа с выражением со скобками
                if(token.equals("(")){
                    stack.push(token); // операторов в стек
                }
                if(token.equals(")")) { // если встретим закрывающую скобку, то пока верхним элементом стека не станет открывающая скобка, выталкиваем элементы из стека в выходную строку
                    while (!(stack.peek().equals("("))) {
                        output.add(stack.pop());
                    }
                    stack.pop(); // уберем '(' скобку
                }


                    if(arithmeticOperators.contains(token)){ // если это оператор


                        while (!stack.isEmpty() && (priority(token)) <= priority(stack.peek())) { //если приоритет текущего оператора ниже, чем приоритет последнего символа в стеке,
                            output.add(stack.pop()); // то последний знак из стека операций мы перемещаем в массив выхода.
                        }

                        stack.push(token);
                    }



                }
            }

        // когда входная строка закончилась
        while (!stack.isEmpty()) { // и если стек еще не пуст
            if (allOperators.contains(stack.peek())) output.add(stack.pop()); // в стеке должны остаться только символы операторов, выталкиваем их

        }
        return output;
    }





}

